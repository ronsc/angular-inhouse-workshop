import { ProductGroup } from './product-group.model';

export class Product {
    id: number;
    code: string;
    name: string;
    unit: string;
    group: ProductGroup;
}
