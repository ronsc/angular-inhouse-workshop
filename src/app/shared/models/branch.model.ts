export class Branch {
    id: number;
    code: string;
    name: string;
    address: string;
    tel: string;
}
