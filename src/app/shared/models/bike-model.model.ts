import { Product } from './product.model';
import { SerialNumber } from './serial-number.model';

export class BikeModel {
    id: number;
    product: Product;
    price: number;
    snList: SerialNumber[];
}
