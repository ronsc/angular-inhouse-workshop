import { Color } from './color.model';
import { Warehouse } from './warehouse.model';
import { Product } from './product.model';

export class SerialNumber {
    id: number;
    engineNumber: string;
    bodyNumber: string;
    color: Color;
    regNumber: string;
    keyNumber: string;
    warehouse: Warehouse;
    product: Product;
}
