import { Branch, BikeModel } from './';

export class Transfer {
    id: number;
    code: string;
    fromBranch: Branch;
    toBranch: Branch;
    bikeModels: BikeModel[];
}
