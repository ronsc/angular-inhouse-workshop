export class Warehouse {
    id: number;
    code: string;
    name: string;
}
