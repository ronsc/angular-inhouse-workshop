import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Color } from '../models';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class ColorService {
  private url = 'api/colors';

  constructor(private http: Http) { }

  getAll(): Observable<Color[]> {
    return this.http.get(this.url)
      .map(res => res.json().data as Color[]);
  }

}
