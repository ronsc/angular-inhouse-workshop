import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Branch } from '../models';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class BranchService {
  private url = 'api/branchs';

  constructor(private http: Http) { }

  getAll(): Observable<Branch[]> {
    return this.http.get(this.url)
      .map(r => r.json().data as Branch[]);
  }

}
