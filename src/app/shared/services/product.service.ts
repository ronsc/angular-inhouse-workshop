import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { ProductGroup, Product } from '../models';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class ProductService {
  private url = 'api/products';

  constructor(private http: Http) { }

  findByGrop(code: string): Observable<Product[]> {
    return this.http.get(`${this.url}?code=^${code}`)
      .map(r => r.json().data as Product[]);
  }

}
