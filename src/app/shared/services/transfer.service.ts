import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Transfer } from '../models';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class TransferService {
  private url = 'api/transfers';

  constructor(private http: Http) { }

  create(transfer: Transfer): Observable<Transfer> {
    return this.http.post(this.url, transfer)
      .map(r => r.json().data as Transfer);
  }

}
