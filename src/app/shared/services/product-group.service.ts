import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { ProductGroup } from '../models';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class ProductGroupService {
  private url = 'api/product-groups';

  constructor(private http: Http) { }

  getAll(): Observable<ProductGroup[]> {
    return this.http.get(this.url)
      .map(r => r.json().data as ProductGroup[]);
  }

}
