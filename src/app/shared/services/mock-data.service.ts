import { InMemoryDbService } from 'angular-in-memory-web-api';

export class MockDataService implements InMemoryDbService {

  createDb() {
    const productgroups = [
      { id: 1, code: 'BL', name: 'Benelli' },
      { id: 2, code: 'HD', name: 'HONDA' },
      { id: 3, code: 'YA', name: 'YAMAHA' }
    ];

    const products = [
      { id: 1, code: 'BL-001', name: 'BN-600W', unit: 'คัน', group: { id: 1, code: 'BL', name: 'Benelli' } },
      { id: 2, code: 'BL-002', name: 'BN135-G', unit: 'คัน', group: { id: 1, code: 'BL', name: 'Benelli' } },
      { id: 3, code: 'HD-001', name: 'W125I', unit: 'คัน', group: { id: 2, code: 'HD', name: 'HONDA' } },
      { id: 4, code: 'HD-002', name: 'ABC600CB', unit: 'คัน', group: { id: 2, code: 'HD', name: 'HONDA' } },
      { id: 5, code: 'YA-001', name: 'YA125(TH)', unit: 'คัน', group: { id: 3, code: 'YA', name: 'YAMAHA' } },
    ];

    const branchs = [
      { id: 1, code: 'BR-001', name: 'หจก. สาขาที่ 1 จำกัด', address: '111 ม.1 ต.ในเมือง อ.ในเมือง จ.ชลบุรี', tel: '0354541123' },
      { id: 2, code: 'BR-002', name: 'หจก. สาขาที่ 2 จำกัด', address: '22/2 ม.2 ต.ในเมือง อ.ในเมือง จ.ชลบุรี', tel: '-' },
      { id: 3, code: 'BR-003', name: 'หจก. สาขาที่ 3 จำกัด', address: '333/3 ม.3 ต.ในเมือง อ.ในเมือง จ.ชลบุรี', tel: '-' },
      { id: 4, code: 'BR-004', name: 'หจก. สาขาที่ 4 จำกัด', address: '44/4 ม.4 ต.ในเมือง อ.ในเมือง จ.ชลบุรี', tel: '03511122233' },
    ];

    const colors = [
      { id: 1, code: 'CO-004', name: 'ขาว' },
      { id: 2, code: 'CO-057', name: 'ขาว-เขียว' },
      { id: 3, code: 'CO-090', name: 'ขาว-เขียว-ดำ' },
      { id: 4, code: 'CO-069', name: 'ขาว-ชมพู' },
      { id: 5, code: 'CO-013', name: 'ขาว-ชมพู-น้ำเงิน' },
    ];

    const warehouses = [
      { id: 1, code: 'ST-001', name: 'หน้าร้าน' },
      { id: 2, code: 'ST-002', name: 'ACB125CBTF(TH)' },
    ];

    const transfers = [];

    return {
      'product-groups': productgroups,
      products,
      branchs,
      colors,
      warehouses,
      transfers
    };
  }

}
