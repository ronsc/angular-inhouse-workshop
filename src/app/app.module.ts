import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TransferModule } from './transfer/transfer.module';
import { CoreModule } from './core/core.module';
import { HeaderComponent } from './core/header.component';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import {
  ColorService,
  MockDataService
} from './shared/services';
import { TransferService } from './shared/services/transfer.service';
import { BranchService } from './shared/services/branch.service';
import { ProductGroupService } from './shared/services/product-group.service';
import { ProductService } from './shared/services/product.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    InMemoryWebApiModule.forRoot(MockDataService),
    CoreModule,
    TransferModule,
    AppRoutingModule
  ],
  providers: [ColorService, TransferService, BranchService, ProductGroupService, ProductService],
  bootstrap: [AppComponent]
})
export class AppModule { }
