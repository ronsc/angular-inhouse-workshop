import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'ws-modal-warehouse',
  templateUrl: './modal-warehouse.component.html',
  styles: []
})
export class ModalWarehouseComponent implements OnInit {
  @ViewChild('modal') modal: ModalDirective;

  constructor() { }

  show() {
    this.modal.show();
  }

  hide() {
    this.modal.hide();
  }

  ngOnInit() {
  }

}
