import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ModalColorComponent } from '../modal-color/modal-color.component';
import { ModalWarehouseComponent } from '../modal-warehouse/modal-warehouse.component';
import { SerialNumber } from '../../shared/models/serial-number.model';
import { Color } from '../../shared/models/color.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'ws-modal-sn',
  templateUrl: './modal-sn.component.html',
  styles: []
})
export class ModalSnComponent implements OnInit {
  @ViewChild('modal') modal: ModalDirective;
  @ViewChild(ModalColorComponent) modalColor: ModalColorComponent;
  @ViewChild(ModalWarehouseComponent) modalWarehouse: ModalWarehouseComponent;

  @Output() added = new EventEmitter<SerialNumber[]>();

  sn: SerialNumber = new SerialNumber();
  form: FormGroup;
  snList: SerialNumber[] = [];
  selectIndex = -1;

  constructor(private fb: FormBuilder) {
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      engineNumber: [this.sn.engineNumber, Validators.required],
      bodyNumber: [this.sn.bodyNumber, Validators.required],
      color: [this.sn.color, Validators.required],
      regNumber: this.sn.regNumber,
      keyNumber: [this.sn.keyNumber, Validators.required],
      warehouse: this.sn.warehouse
    });
  }

  show() {
    this.modal.show();
  }

  hide() {
    this.modal.hide();
  }

  save() {
    this.added.emit(this.snList);
    this.hide();
    this.form.reset();
  }

  add() {
    this.sn = this.form.value;
    if (this.selectIndex === -1) {
      this.snList.push(this.sn);
    } else {
      this.snList[this.selectIndex] = this.sn;
    }
    this.form.reset();
    this.selectIndex = -1;
  }

  showColor() {
    this.modalColor.show();
  }

  showWarehouse() {
    this.modalWarehouse.show();
  }

  setColor(color: Color) {
    this.form.patchValue({ color: color });
    this.sn.color = color;
  }

  onSelect(sn: SerialNumber, index: number) {
    this.form.patchValue(sn);
    this.selectIndex = index;
  }

  onDelete(index: number) {
    this.snList.splice(index, 1);
  }

  onDeleteAll() {
    this.snList = [];
  }

  ngOnInit() {
  }

}
