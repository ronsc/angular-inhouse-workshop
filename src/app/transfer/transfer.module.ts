import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { ModalModule } from 'ngx-bootstrap/modal';
import { AlertModule } from 'ngx-bootstrap/alert';

import { TransferRoutingModule } from './transfer-routing.module';
import { TransferComponent } from './transfer.component';
import { ModalBranchComponent } from './modal-branch/modal-branch.component';
import { ModalProductComponent } from './modal-product/modal-product.component';
import { ModalProductSearchComponent } from './modal-product-search/modal-product-search.component';
import { ModalSnComponent } from './modal-sn/modal-sn.component';
import { ModalColorComponent } from './modal-color/modal-color.component';
import { ModalWarehouseComponent } from './modal-warehouse/modal-warehouse.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    AlertModule.forRoot(),
    TransferRoutingModule
  ],
  declarations: [
    TransferComponent,
    ModalBranchComponent,
    ModalProductComponent,
    ModalProductSearchComponent,
    ModalSnComponent,
    ModalColorComponent,
    ModalWarehouseComponent
  ]
})
export class TransferModule { }
