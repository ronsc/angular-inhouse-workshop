import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { BranchService } from '../../shared/services/branch.service';
import { Branch } from '../../shared/models';

@Component({
  selector: 'ws-modal-branch',
  templateUrl: './modal-branch.component.html',
  styles: []
})
export class ModalBranchComponent implements OnInit {
  @ViewChild('branchModal') private branchModal: ModalDirective;

  @Output() selected = new EventEmitter<any>();

  branchs: Branch[] = [];

  constructor(private branchService: BranchService) { }

  onSelected(item) {
    this.selected.emit(item);
    this.hide();
  }

  show() {
    this.branchModal.show();
  }

  hide() {
    this.branchModal.hide();
  }

  ngOnInit() {
    this.branchService.getAll()
      .subscribe(branchs => this.branchs = branchs);
  }

}
