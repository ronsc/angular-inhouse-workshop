import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ProductGroupService } from '../../shared/services/product-group.service';
import { ProductGroup, Product } from '../../shared/models';
import { ProductService } from '../../shared/services/product.service';

@Component({
  selector: 'ws-modal-product-search',
  templateUrl: './modal-product-search.component.html',
  styles: []
})
export class ModalProductSearchComponent implements OnInit {
  @ViewChild('searchProductModal') searchProductModal: ModalDirective;

  @Output() selected = new EventEmitter<any>();

  productGroups: ProductGroup[] = [];
  products: Product[] = [];

  constructor(private prodGroupService: ProductGroupService, private prodService: ProductService) { }

  selectGroup(code: string) {
    this.prodService.findByGrop(code)
      .subscribe(datas => this.products = datas);
  }

  selectProduct(item) {
    this.selected.emit(item);
    this.hide();
  }

  show() {
    this.searchProductModal.show();
  }

  hide() {
    this.searchProductModal.hide();
  }

  ngOnInit() {
    this.prodGroupService.getAll()
      .subscribe(datas => {
        this.productGroups = datas;
        // Make Select First Group
        this.selectGroup(datas[0].code);
      });
  }

}
