import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { ModalDirective } from 'ngx-bootstrap/modal';

import { ModalBranchComponent } from './modal-branch/modal-branch.component';
import { ModalProductComponent } from './modal-product/modal-product.component';
import { BikeModel, Transfer, Branch } from '../shared/models';
import { TransferService } from '../shared/services/transfer.service';

@Component({
  selector: 'ws-transfer',
  templateUrl: './transfer.component.html',
  styles: []
})
export class TransferComponent implements OnInit {
  @ViewChild(ModalBranchComponent) modalBranch: ModalBranchComponent;
  @ViewChild(ModalProductComponent) modalProduct: ModalProductComponent;

  transfer: Transfer = new Transfer();
  form: FormGroup;
  alert = {
    type: 'success',
    msg: `บันทึกข้อมูลสำเร็จ.`,
    open: false
  };

  constructor(private fb: FormBuilder, private transferService: TransferService) {
    this.transfer.bikeModels = [];
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      toBranch: this.fb.group({
        code: ['', Validators.required],
        name: [''],
        address: [''],
        tel: [''],
      })
    });
  }

  onSubmit() {
    this.transferService.create(this.transfer)
      .subscribe(data => {
        this.alert.open = true;
        this.form.reset();
        this.transfer = new Transfer();
      });
  }

  showBranchModal() {
    this.modalBranch.show();
  }

  showProductModal() {
    this.modalProduct.show();
  }

  setBranch(branch: Branch) {
    this.form.patchValue({ toBranch: branch });
    this.transfer.toBranch = branch;
  }

  setBikeModel(bikeModel: BikeModel) {
    this.transfer.bikeModels.push(bikeModel);
  }

  ngOnInit() {
  }

}
