import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ColorService } from '../../shared/services';
import { Color } from '../../shared/models/color.model';

@Component({
  selector: 'ws-modal-color',
  templateUrl: './modal-color.component.html',
  styles: []
})
export class ModalColorComponent implements OnInit {
  @ViewChild('modal') modal: ModalDirective;

  @Output() selected = new EventEmitter<Color>();

  colors: Color[];

  constructor(private colorService: ColorService) { }

  show() {
    this.modal.show();
  }

  hide() {
    this.modal.hide();
  }

  onSelect(color: Color) {
    this.selected.emit(color);
    this.hide();
  }

  ngOnInit() {
    this.colorService.getAll()
      .subscribe(data => this.colors = data);
  }

}
