import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ModalProductSearchComponent } from '../modal-product-search/modal-product-search.component';
import { ModalSnComponent } from '../modal-sn/modal-sn.component';
import {
  SerialNumber,
  BikeModel
} from '../../shared/models';

@Component({
  selector: 'ws-modal-product',
  templateUrl: './modal-product.component.html',
  styles: []
})
export class ModalProductComponent implements OnInit {
  @ViewChild('productModal') productModal: ModalDirective;
  @ViewChild(ModalProductSearchComponent) modalProductSearch: ModalProductSearchComponent;
  @ViewChild(ModalSnComponent) modalSN: ModalSnComponent;

  @Output() added = new EventEmitter<BikeModel>();

  bikeModel: BikeModel = new BikeModel();
  form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.bikeModel.snList = [];
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      product: [null, Validators.required],
      price: ['', Validators.required],
      snList: [null, Validators.required]
    });
  }

  save() {
    this.bikeModel = this.form.value;
    this.added.emit(this.bikeModel);
    this.hide();
    this.form.reset();
  }

  show() {
    this.productModal.show();
  }

  hide() {
    this.productModal.hide();
  }

  showProductSearchModal() {
    this.modalProductSearch.show();
  }

  showSNModal() {
    this.modalSN.show();
  }

  setProduct(item) {
    this.form.patchValue({ product: item });
  }

  setSNList(snList: SerialNumber[]) {
    this.form.patchValue({ snList: snList });
  }

  get total() {
    if (!this.form.get('snList').value) {
      return;
    }
    return this.form.get('snList').value.length * this.form.get('price').value;
  }

  ngOnInit() {
  }

}
